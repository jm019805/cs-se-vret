/*
 * vret.cxx
 * 
 * Copyright 2023 Soul <so.st@tuta.io>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <string>
#include <cstring>
#include <limits>

#define MAXPT 5			// Max patients databse can store
#define MAXCHAR	100

using namespace std;

string getInput (string prompt);

int checkPIDinRange(int PID){
		int inRange = PID >= 0 && PID <= MAXPT;
		if (!inRange){
				cout	<< "PID " << PID << " is not in range" << endl;
		}
		return inRange;
}

class auth {
		public:
		
			static int check (int level = 0){
				char input [MAXCHAR];
				cout	<<	"[AUTH] Please enter " << (level?"admin password: ":"password: ");
				cin.getline(input, MAXCHAR);
						
				int valid = input == (level?admin_password:password);
				if (!valid)
					cout	<< "[AUTH] Error No Access"	<< endl;
				else
					cout	<< "[AUTH] Access Granted"	<< endl;
				
				return valid;
			}
			
		private:
			static string password;
			static string admin_password;
};
string auth::password = "password";
string auth::admin_password = "admin";

class pt {
	public:
		int PID;
		
		int __init__ (string n, string d, string e, string m, string b){
			if (!auth::check()) return 1;
			
			name		= n;
			DOB			= d;
			email		= e;
			mobileNo	= m;
			bloodGroup	= b;
			
			return 0;
		}
		
		int printInfo(){
			if (!auth::check()) return 1;
			
			cout	<<	"___Patient "<< PID	<< " info___"	<<endl;
			
			cout	<<	"Name:\t\t"			<<	name		<< endl;	
			cout	<<	"DOB:\t\t"			<<	DOB			<< endl;	
			cout	<<	"E-mail:\t\t"		<<	email		<< endl;	
			cout	<<	"Mobile Number:\t"	<<	mobileNo	<< endl;	
			cout	<<	"Blood Type\t"		<<	bloodGroup	<< endl;
			
			return 0;	
		}
		
		int editInfo(){
			// Check authority
			if (!auth::check(1)) return 1;
			
			// Display prompts
			cout	<<	"___UPDATE Patient's "<< PID	<< " info___"	<<endl;
			cout	<<	"PLease select a form to update" << endl;
			
			string forms[5] = {"Name", "DOB", "E-mail", "Mobile Number", "Bloodtype"};
			for (int i=0;i<5;i++)
					cout	<< "[" << i << "] " << forms[i] << "...\n";
					
			// Take input
			int opt;
			string input = getInput("Select option...");
			opt = stoi(input);
			
			// Error handling
			if (opt > 4 || opt < 0){
				cout << opt << " is not a valid option\n";
				return 2;
			}
			
			// Take new data

			input = getInput("Enter updated");
			
			switch (opt){
					case 0:
						name = input;
						break;
					case 1:
						DOB = input;
						break;
					case 2:
						email = input;
						break;
					case 3:
						mobileNo = input;
						break;
					case 4:
						bloodGroup = input;
						break;
			}
			
			return 0;
		}
		
		int removeInfo(){
			cout	<<	"___!!REMOVE!! Patient "<< PID	<< " info___"	<<endl;
			if (!auth::check()) return 1;
			name = "";
			DOB = "";
			email = "";
			mobileNo = "";
			bloodGroup = "";
			return 0;
		}
	
	private:
		string name;
		string DOB;
		string email;
		string mobileNo;
		string bloodGroup;
};

pt patients [MAXPT];	// Array to store all patient data
int ptIdx = 0;			// Keep track of PID for new patients

string getInput (string prompt){
		char input [MAXCHAR];
		cout	<<	prompt	<< ":\t";
		cin.getline(input, MAXCHAR);
		
		return input;
}


int main(int argc, char **argv)
{	
	while (1){
		int opt;
		cout	<<	"Please select an option..."	<< endl;
		cout	<<	"[1] Create new patient"	<< endl;
		cout	<<	"[2] View Patient Info"	<< endl;
		cout	<<	"[3] Edit Patient Info"	<< endl;
		cout	<<	"[4] REMOVE Patient Info"	<< endl;
		cin		>>	opt; getchar();
		
		switch (opt){
				case 1:{
					cout	<< "___New Patient___"	<< endl;
					
					int PID;
					string name, DOB, email, mobileNo, bloodGroup;

					PID			= ptIdx++;
					name		= getInput("Name");
					DOB			= getInput("DOB");
					email		= getInput("E-mail");
					mobileNo	= getInput("Mobile Number");
					bloodGroup	= getInput("Bloodtype");
					
					pt patient;
					patient.PID = PID;
					if (patient.__init__(name,DOB,email,mobileNo,bloodGroup) == 0)
						patients[PID++] = patient;
							
					
					break;
				}
				case 2:{
					int PID;
					string input = getInput("Enter Patient PID");
					PID = stoi(input);
					
					patients[PID].printInfo();
					break;
				}
				
				case 3:{
					int PID;
					string input = getInput("Enter Patient PID");
					PID = stoi(input);
					
					if (checkPIDinRange(PID) == 0)
						break;
					
					patients[PID].editInfo();
					break;
				}
				
				case 4:{
					int PID;
					string input = getInput("Enter Patient PID");
					PID = stoi(input);
					patients[PID].removeInfo();
					break;
				}
			
		}
		
	}
	
	return 0;
}



